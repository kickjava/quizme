<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="login.jsp">Home</a></li>
	    		<li class="active"><a href="test">Quizes</a></li>);
	          </ul>
	          <form class="navbar-form navbar-right" style="color:#808080;">
		        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
		        	<button class="btn btn-success"><a href="Register">Logout</a></button>
	        	</form>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
	    
		<div class="container">
		<% try {  %>
			<form name="editTest" method="post" action="ModifyTest" style="width: 300px;">
				<input type="hidden"  name="testID" value="<%=request.getParameter("testID")%>">
				Name: <input name="testName" class="form-control" autofocus c:out value="<%= request.getParameter("Name")%>"/>
				Description: <input name="testDescription" " class="form-control" type="text" c:out value="<%= request.getParameter("Desc")%>"/>
				<br />
				<button type="submit" class="btn btn-lg btn-primary btn-block">Update</button>
			</form>
			<% } catch(Exception e) { response.sendRedirect("test.jsp"); } %>
		</div>
	</body>
</html>