<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li class="active"><a href="login.jsp">Home</a></li>
	            <% if(session.getAttribute("username") != null){ %>
	            	<li><a href="test">Quizes</a></li>
	            <% } %>
	          </ul>
	          	<% if(session.getAttribute("username") != null){ %>
		          	<form class="navbar-form navbar-right" style="color:#808080;">
			        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
			        	<button class="btn btn-success" onclick="location.href='Register'">Logout</button>
		        	</form>
	        	<% } %>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
	    
	    <div class="container">
			<form class="form-signin" name="loginForm" method="post" action="LoginServlet" style="width:350px;">
				<h2 class="form-signin-heading">Please sign in</h2>
				 <label for="inputEmail" class="sr-only">Email address</label>  
				<input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="username" autofocus c:out value="${param.username}"/>
				<%
				if((String)request.getAttribute("usernameError") == null){
					out.println("");
				}
				else{ %>
					<%=(String)request.getAttribute("usernameError")%>
				<% } %>
				
				<label for="inputPassword" class="sr-only">Password</label>
				<input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" />
				<%
				if((String)request.getAttribute("passwordError") == null){
					out.println("");
				}
				else{ %>
					<%=(String)request.getAttribute("passwordError")%>
				<% } %>
				
				
				<label>
            		<a href="register.jsp">Register now!</a>
          		</label>
				<button type="submit" class="btn btn-lg btn-primary btn-block">Sign in</button> 
			</form>
		</div> <!-- /container -->
		<!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="js/bootstrap.min.js"></script>
	</body>
</html>