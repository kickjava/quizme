<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.List"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
	
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="login.jsp">Home</a></li>
	    		<li class="active"><a href="test">Quizes</a></li>);
	          </ul>
	          <form class="navbar-form navbar-right" style="color:#808080;">
		        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
		        	<button class="btn btn-success" onclick="location.href='Register'">Logout</button>
	        	</form>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
	    
	    <% if(session.getAttribute("role") == null){
					response.sendRedirect("login.jsp");
			}
			if(!session.getAttribute("role").equals("PROFESSOR")){
					response.sendRedirect("403.html");
			} 
			List<Double> alltests = (List<Double>)request.getAttribute("results");
			double sum = 0;
			for(double x : alltests){
				sum += x;
			}
			
			double median = 0;
			int middle = ((alltests.size()) / 2);
			if(alltests.size() % 2 == 0){
				 double medianA = alltests.get(middle);
				 double medianB = alltests.get(middle - 1);
				 median = (medianA + medianB) / 2;
				} else{
				 median = alltests.get(middle + 1);
				}
			%>
			
		<div class="container">
			<h3>Test Stats</h3>
			<div class="row">
				<div class="col-md-1">Mean:</div>
				<div class="col-md-1"><%= sum / 6 %></div>
			</div>
			
			<div class="row">
				<div class="col-md-1">Median:</div>
				<div class="col-md-1"><%= median %></div>
			</div>
			
			<div class="row">
				<div class="col-md-1">Range:</div>
				<div class="col-md-1"><% out.print(alltests.get(alltests.size() - 1) - alltests.get(0)); %></div>
			</div>
		</div>		
	</body>
</html>