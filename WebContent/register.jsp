<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="login.jsp">Home</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
	    
		<div class="container">
		<b>Register</b>
		<form name="registerForm" method="post" action="RegisterServlet">
			Full Name: <input type="text" name="name" autofocus />
			<%
			if((String)request.getAttribute("nameError") == null){
				out.println("");
			}
			else{ %>
				<%=(String)request.getAttribute("nameError")%>
			<% } %>
			<br />
			Email: <input type="email" name="email" />
			<%
			if((String)request.getAttribute("emailError") == null){
				out.println("");
			}
			else{ %>
				<%=(String)request.getAttribute("emailError")%>
			<% } %>
			<br />
			Password: <input type="password" name="password" />
			<%
			if((String)request.getAttribute("passwordError") == null){
				out.println("");
			}
			else{ %>
				<%=(String)request.getAttribute("passwordError")%>
			<% } %>
			<br />
			
			User Type: <select name="type"> 
							<option value="Student">Student</option>
							<option value="Professor">Professor</option>
						</select> 
			<br />
			<%
			if((String)request.getAttribute("persistError") == null){
				out.println("");
			}
			else{ %>
				<%=(String)request.getAttribute("persistError")%>
			<% } %>
			
			<input type="submit" value="Register" /> &nbsp; &nbsp;
			<a href="login.jsp">Login</a>
		</form>
		</div>
	</body>
</html>