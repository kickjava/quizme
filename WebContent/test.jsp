<%@page import="java.util.List, dbj565_a1.model.Tests"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head> 
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
	
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="login.jsp">Home</a></li>
	    		<li class="active"><a href="test">Quizes</a></li>);
	          </ul>
	          <form class="navbar-form navbar-right" style="color:#808080;">
		        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
		        	<button class="btn btn-success" onclick="location.href='Register'">Logout</button>
	        	</form>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
	    	
		<%	try { if(session.getAttribute("role") == null){
					response.sendRedirect("login.jsp");
				}
			if(!session.getAttribute("role").equals("PROFESSOR")){
					response.sendRedirect("403.html");
				} %>
					
		<div class="container">
			<form class="form-horizontal" name="createViewTests" method="post" action="TestServlet">
				<h3 class="form-signin-heading">Create Quiz</h3>
				<div class="form-group">
					<label for="InputName" class="col-sm-1 control-label">Name</label>
					<div class="col-sm-6">
						<input name="testName" id="InputName" required autofocus c:out value="${param.testName}"/>
					</div>
					<div class="col-sm-6">
						<%
						if((String)request.getAttribute("testNameError") == null){
							out.println("");
						}   
						else{ 
							out.println((String)request.getAttribute("testNameError"));
						} %>
					</div>
				</div>
				<div class="form-group">
					<label for="InputDesc" class="col-sm-1 control-label">Description</label>
					<div class="col-sm-6">
						<input name="testDescription" id="InputDesc" type="text" required c:out value="${param.testDescription}"/>
					</div>
					<div class="col-sm-6">
						<% 
						if((String)request.getAttribute("descriptionError") == null){
							out.println("");    
						}
						else{ 
							out.println((String)request.getAttribute("descriptionError"));
						} %>
					</div>
				</div>	
				<div class="form-group">
				    <div class="col-sm-offset-1 col-sm-2">
						<button type="submit" class="btn btn-lg btn-primary btn-block">Create</button>
					</div>
				</div> 
			</form>
			<hr>
			
			<% if((String)request.getAttribute("message") == null){ out.println(""); } 
				else {%> <p><%=(String)request.getAttribute("message")%></p> <% } %>
			
			<h3>All Quizes</h3>
			<div class="row">
				<div class="col-md-1">
					<b>Name</b>
				</div>
				<div class="col-md-3">
					<b>Description</b>
				</div>
				<div class="col-md-1">
					<b>Publish</b>
				</div>
				<hr>
			</div>
			
			<% @SuppressWarnings("unchecked")
			List<Tests> list = (List<Tests>) request.getAttribute("testList"); 
			for(Tests t : list){  %>
				<div class="row">
					<div class="col-md-1"><%= t.getName() %></div>
					<div class="col-md-3"><%= t.getDescription() %></div>
					<div class="col-md-1"><% if(t.isPublish()){ out.println("Yes"); } else { out.println("No"); } %></div>
					<div class="col-md-2"><a href="createQuestion.jsp?testID=<%= t.getId() %>">Add Question</a></div>
					<div class="col-md-1"><a href="editTest.jsp?testID=<%= t.getId() %>&Name=<%= t.getName()%>&Desc=<%= t.getDescription()%>">Edit</a></div>
					<div class="col-md-2"><a href="modifyTest?action=publish&testID=<%= t.getId() %>">Publish/Unpublish</a></div>
					<div class="col-md-1"><a href="modifyTest?action=delete&testID=<%= t.getId() %>">Delete</a></div>
					<div class="col-md-1"><a href="proffResult?testID=<%= t.getId() %>">Score</a></div>
					<hr />
				</div>
			<% } %>
				
		</div>
		<% } catch (Exception e) { System.out.println("Exception is : " + e.getMessage());
			response.sendRedirect("login.jsp"); }  %>
	</body>
</html>