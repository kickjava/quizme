<%@page import="java.util.List, dbj565_a1.model.Questions, javax.servlet.RequestDispatcher"%>
<%@page import="dbj565_a1.model.Options, dbj565_a1.model.Difficulty"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>Quiz Me</title>
	</head>
	<body>
	<%
	if(session.getAttribute("role") == null){
		response.sendRedirect("login.html"); 
	}
	if(request.getAttribute("listOfQuestions") == null) {
		response.sendRedirect("test");
	}
	 @SuppressWarnings("unchecked")
		List<Questions> allQuestions = (List<Questions>) request.getAttribute("listOfQuestions");
		int qNumber = 0;
	    if(session.getAttribute("qNum") != null){
	    	qNumber = (Integer) session.getAttribute("qNum");
	    	//out.println("qNum: " + qNumber);
	    }
	    else{
			qNumber = (Integer) session.getAttribute("qNumber");
			//out.println("qNumber");
	    }
	    
	    //Passing quesiton list back
	    session.setAttribute("questionList", allQuestions);
	    session.setAttribute("qNum",qNumber);
	    session.setAttribute("test",request.getParameter("testID"));
%> 	 
	<div class="container">   
		<h3>Questions</h3>
		<form name="questionList" method="post" action="takeQuiz">
			<div class="row">
				<div class="col-md-2"> Type: </div>
			   	<div class="col-md-6"><%=allQuestions.get(qNumber).getType()%><br></div>
			 </div>
			   <% Difficulty d = allQuestions.get(qNumber).getDifficultyID(); %>
			<div class="row">
				<div class="col-md-2"> Difficulty: </div>
			    <div class="col-md-6"><%=d.getName()%><br></div>
			</div>
			<div class="row">
				<div class="col-md-2"> Question # <%=qNumber+1%>: </div>
			    <div class="col-md-6"><%=allQuestions.get(qNumber).getQuestion()%><br></div>
			</div>
			<div class="row">
				<div class="col-md-2"> Hint: </div>
				 <div class="col-md-6"><%= allQuestions.get(qNumber).getHint() %> <br></div>
			</div>
			<div class="row">
				<div class="col-md-2"> Answer: </div>
			   
		    	<% if(allQuestions.get(qNumber).getType().equals("text")){ %>
		    	   		 <div class="col-md-6"><input type="text" name="answer" size="90" autofocus /></div>
		    	   		 </div> 
				<% } 
		    	else if(allQuestions.get(qNumber).getType().equals("number")){ %>
		    			 <div class="col-md-6"><input type="number" name="answer" size="90"  autofocus /></div>
		    			 </div>
		    	<% }
		    	   else if(allQuestions.get(qNumber).getType().equals("select")){ 
						List<Options> l = allQuestions.get(qNumber).getOptions();
						//java.util.Collections .shuffle(l);
					%>
						 <div class="col-md-6">
						 	<select name="answer">
							 	<option value="<%= l.get(0).getOptions() %>"><%= l.get(0).getOptions() %></option> 
								<option value="<%= l.get(1).getOptions() %>"><%= l.get(1).getOptions() %></option>
								<option value="<%= l.get(2).getOptions() %>"><%= l.get(2).getOptions() %></option>
								<option value="<%= l.get(3).getOptions() %>"><%= l.get(3).getOptions() %></option>	
							</select> 
						</div>
						</div>
			    	<% } 
		    	   else if(allQuestions.get(qNumber).getType().equals("mcq")){ 
		    		   List<Options> l = allQuestions.get(qNumber).getOptions();
					   java.util.Collections .shuffle(l);
		    	   %>
		    	   	<div class="col-md-6">
		    		   <input type="radio" name="answer" value="<%= l.get(0).getOptions() %>" /> <%= l.get(0).getOptions() %> <br /> 
		    		   <input type="radio" name="answer" value="<%= l.get(1).getOptions() %>" /> <%= l.get(1).getOptions() %> <br />
		    		   <input type="radio" name="answer" value="<%= l.get(2).getOptions() %>" /> <%= l.get(2).getOptions() %> <br />
		    		   <input type="radio" name="answer" value="<%= l.get(3).getOptions() %>" /> <%= l.get(3).getOptions() %> <br />
		    		</div>
		    		</div>   
		    	   <% } 
		    	   else if(allQuestions.get(qNumber).getType().equals("checkbox")){ 
		    		   List<Options> l = allQuestions.get(qNumber).getOptions();
					   //java.util.Collections .shuffle(l);	
		    	   %> 
		    	   <div class="col-md-6">
		    		   <input type="checkbox" name="answer" value="OptionA" /> <%= l.get(0).getOptions() %> <br /> 
		    		   <input type="checkbox" name="answer" value="OptionB" /> <%= l.get(1).getOptions() %> <br /> 
		    		   <input type="checkbox" name="answer" value="OptionC" /> <%= l.get(2).getOptions() %> <br /> 
		    		   <input type="checkbox" name="answer" value="OptionD" /> <%= l.get(3).getOptions() %> <br />
		    		</div>
		    		</div>
		    	   <% } %>
			   <br/ ><input type="submit" name="Submit Answer" value="Submit Answer" />
		</form>
	</div>
	</body>
</html>