<%@page import="java.util.List, dbj565_a1.model.Questions, javax.servlet.RequestDispatcher"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
		<% try { if(!session.getAttribute("role").equals("PROFESSOR")){
					RequestDispatcher rd = request.getRequestDispatcher("403.html");
					rd.forward(request, response);		
					//response.sendRedirect("403.html");
				}
			} catch (Exception e) { //response.sendRedirect("login.html"); 
				request.getRequestDispatcher("login.html").forward(request, response);	}
		
			if(request.getParameter("testID") == null) {
				//response.sendRedirect("test");
				RequestDispatcher rd = request.getRequestDispatcher("/test");
				rd.forward(request, response);
			} 
			
			if(request.getAttribute("questionPool") == null) {
				request.setAttribute("testID", request.getParameter("testID"));
				
				RequestDispatcher rd = request.getRequestDispatcher("/createQuestion");
				rd.forward(request, response);
			}%>
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="login.jsp">Home</a></li>
	    		<li class="active"><a href="test">Quizes</a></li>);
	          </ul>
	          <form class="navbar-form navbar-right" style="color:#808080;">
		        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
		        	<button class="btn btn-success" onclick="location.href='Register'">Logout</button>
	        	</form>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
		
		<div class="container">	
			<form class="form-horizontal" action="createQuestion" method="POST" style="width:800px;">
				<% request.setAttribute("testID", request.getParameter("testID"));  %>
				<h3 class="form-signin-heading">Create Question</h3>
				<input type="hidden" name="testID" value="<%=(String)request.getAttribute("testID")%>">
				
				<div class="form-group">
					<label for="questionType" class="col-sm-2 control-label">Type</label>
					<div class="col-sm-6">
						<select id="questionType" class="form-control" name="questionChoose" onChange="TypeChanged()"> 
							<option value="text">Text Input</option>
							<option value="number">Numeric Input</option>
							<option value="mcq">Multiple Choices</option>
							<option value="checkbox">Checkboxes</option>
							<option value="select">Dropdown</option>
						</select>
					</div>
				</div>	
				<div class="form-group">
					<label for="diffType" class="col-sm-2 control-label">Difficulty</label>
					<div class="col-sm-6">
						<select name="difficult" id="diffType" class="form-control">
							<option value="easy" selected>Easy</option>
							<option value="medium">Medium</option>
							<option value="hard">Hard</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputquestion" class="col-sm-2 control-label">Question</label>
					<div class="col-sm-6">
						<input type="text" id="inputquestion" name="question" size="50" />
					</div>
				</div>
				<% if((String)request.getAttribute("questionError") == null){
						out.println("");
					} else { %>
						<%=(String)request.getAttribute("questionError")%>
				<% } %>
						
				<div class="form-group">
					<label for="inputhint" class="col-sm-2 control-label">Hint</label>
					<div class="col-sm-6">
						<input type="text" id="inputhint" name="hint" size="50" />
					</div>
				</div>
			
				<span id="options"></span> 
				
				<% if((String)request.getAttribute("optionError") == null){ out.println(""); }
				else{ %> <%=(String)request.getAttribute("optionError")%> <% } %>
				
				<div class="form-group">
					<label for="inputAnswer" class="col-sm-2 control-label">Answer</label>
					<div class="col-sm-6">
						<span id="answerType"><input type="text" name="answer" size="90" /></span>
					</div>
				</div>
				
				<% if((String)request.getAttribute("answerError") == null){
						out.println("");
					} else { %>
						<%=(String)request.getAttribute("answerError")%>
				<% } %>
				
				<script language="Javascript">
					function TypeChanged(){
						var x = document.getElementById("questionType").value;
					    //document.getElementById("answerType").innerHTML = "You selected: " + x;
					    switch(x){
						    case "text": 
						    	document.getElementById('answerType').innerHTML = '<input type="text" name="answer" size="90" />';
						    	document.getElementById('options').innerHTML = '';
						    	break;
						    case "number": 
						    	document.getElementById('answerType').innerHTML = '<input type="number" name="answer" />';
						    	document.getElementById('options').innerHTML = '';
						    	break;
						    case "mcq": 
						    	document.getElementById('options').innerHTML = '<div class="form-group"> <label for="optionA" class="col-sm-2 control-label">Option A</label> <div class="col-sm-6"> <input type="text" id="optionA" name="optionA" /> </div> </div>'
							    	+ '<div class="form-group"> <label for="optionB" class="col-sm-2 control-label">Option B</label> <div class="col-sm-6"> <input type="text" id="optionB" name="optionB" /> </div> </div>'
							    	+ '<div class="form-group"> <label for="optionC" class="col-sm-2 control-label">Option C</label> <div class="col-sm-6"> <input type="text" id="optionC" name="optionC" /> </div> </div>'
									+ '<div class="form-group"> <label for="optionD" class="col-sm-2 control-label">Option D</label> <div class="col-sm-6"> <input type="text" id="optionD" name="optionD" /> </div> </div>';
						    	document.getElementById('answerType').innerHTML = '<div class="form-group"><input type="radio" name="answer" value="OptionA" /> Option A &nbsp; <input type="radio" name="answer" value="OptionB" /> Option B &nbsp; <input type="radio" name="answer" value="OptionC" /> Option C &nbsp; <input type="radio" name="answer" value="OptionD" /> Option D </div>';
						    	break;
						    case "checkbox": 
						    	document.getElementById('options').innerHTML = '<div class="form-group"> <label for="optionA" class="col-sm-2 control-label">Option A</label> <div class="col-sm-6"> <input type="text" id="optionA" name="optionA" /> </div> </div>'
							    	+ '<div class="form-group"> <label for="optionB" class="col-sm-2 control-label">Option B</label> <div class="col-sm-6"> <input type="text" id="optionB" name="optionB" /> </div> </div>'
							    	+ '<div class="form-group"> <label for="optionC" class="col-sm-2 control-label">Option C</label> <div class="col-sm-6"> <input type="text" id="optionC" name="optionC" /> </div> </div>'
									+ '<div class="form-group"> <label for="optionD" class="col-sm-2 control-label">Option D</label> <div class="col-sm-6"> <input type="text" id="optionD" name="optionD" /> </div> </div>';
						    	document.getElementById('answerType').innerHTML = '<input type="checkbox" name="answer" value="OptionA" /> Option A &nbsp; <input type="checkbox" name="answer" value="OptionB" /> Option B &nbsp; <input type="checkbox" name="answer" value="OptionC" /> Option C &nbsp; <input type="checkbox" name="answer" value="OptionD" /> Option D';
						    	break;
						    case "select": 
						    	document.getElementById('options').innerHTML = '<div class="form-group"> <label for="optionA" class="col-sm-2 control-label">Option A</label> <div class="col-sm-6"> <input type="text" id="optionA" name="optionA" /> </div> </div>'
							    	+ '<div class="form-group"> <label for="optionB" class="col-sm-2 control-label">Option B</label> <div class="col-sm-6"> <input type="text" id="optionB" name="optionB" /> </div> </div>'
							    	+ '<div class="form-group"> <label for="optionC" class="col-sm-2 control-label">Option C</label> <div class="col-sm-6"> <input type="text" id="optionC" name="optionC" /> </div> </div>'
									+ '<div class="form-group"> <label for="optionD" class="col-sm-2 control-label">Option D</label> <div class="col-sm-6"> <input type="text" id="optionD" name="optionD" /> </div> </div>';
						    	document.getElementById('answerType').innerHTML = '<select name="answer"class="form-control"> <option value="OptionA">Option A</option> <option value="OptionB">Option B</option> <option value="OptionC">Option C</option> <option value="OptionD">Option D</option></select>';
						    	break;
					    }
					}
				</script>	
				<br />
				<!-- <span id="choiceMsg"></span> 
				<span id="numOfOptions">&nbsp;</span>  -->
				<!-- Choices: <div id="choices-div"></div> -->
				<div class="form-group">
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-6">
						<button type="submit" class="btn btn-lg btn-primary btn-block" style="width:400px">Create Question</button>
					</div>
				</div>
							
			</form>
			<hr>
			
			<h3>All Questions</h3>
			<div class="row">
				<div class="col-md-6">
					<b>Question</b>
				</div>
				<div class="col-md-1">
					<b>Type</b>
				</div>
				<div class="col-md-1">
					<b>Difficulty</b>
				</div>
				<hr>
			</div>
			
			<% @SuppressWarnings("unchecked")
				List<Questions> allQuestions = (List<Questions>) request.getAttribute("questionPool");  %>
			
				<% for(Questions q : allQuestions) { %>
					<div class="row">
						<div class="col-md-6"><%= q.getQuestion() %></div>
						<div class="col-md-1"><%= q.getType() %></div>
						<div class="col-md-1"><%= q.getDifficultyID().getName() %></div>
						<div class="col-md-1"><a href="deleteQuestion?testID=<%= q.getTestID().getId() %>&id=<%= q.getId() %>">Delete</a></div>
						<hr />
					</div>
				<% } %>	
			
		</div>
		<% /*} catch(Exception e) { System.out.println(e.getMessage());
			response.sendRedirect("login.jsp"); } */ %>
	 
	</body>
</html>