<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="dbj565_a1.model.Results"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li class="active"><a href="login.jsp">Home</a></li>
	            <% if(session.getAttribute("username") != null){ %>
	            	<li><a href="test">Quizes</a></li>
	            <% } %>
	          </ul>
	          	<% if(session.getAttribute("username") != null){ %>
		          	<form class="navbar-form navbar-right" style="color:#808080;">
			        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
			        	<button class="btn btn-success" onclick="location.href='Register'">Logout</button>
		        	</form>
	        	<% } %>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />
	
		<div class="container">
			Test: <%= request.getAttribute("test") %> <br />
			Score: <%= Double.parseDouble(request.getAttribute("result").toString()) / 6 * 100 %> <br />
		</div>
	</body>
</html>