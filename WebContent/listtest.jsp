<%@page import="java.util.List, dbj565_a1.model.Tests" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Bootstrap -->
    	<link href="css/bootstrap.min.css" rel="stylesheet">
		<title>QuizMe</title>
	</head>
	<body>	
		<% if(session.getAttribute("userID") == null){ //checking if logged in..
			response.sendRedirect("login.html"); } %>	
		<nav class="navbar navbar-inverse navbar-fixed-top">
	      <div class="container">
	        <div class="navbar-header">
	          <a class="navbar-brand" href="login.jsp">QuizMe</a>
	        </div>
	        <div id="navbar" class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="login.jsp">Home</a></li>
	    		<li class="active"><a href="test">Quizes</a></li>);
	          </ul>
	          <form class="navbar-form navbar-right" style="color:#808080;">
		        	Welcome, <%= (String)session.getAttribute("username") + "  "%> 
		        	<button class="btn btn-success" onclick="location.href='Register'">Logout</button>
	        	</form>
	        </div><!--/.nav-collapse -->
	      </div>
	    </nav>
	    <br />
	    <br />
	    <br />			
		
		<div class="container">
		<h3>All Quizes</h3>
		<div class="row">
				<div class="col-md-2">
					<b>Name</b>
				</div>
				<div class="col-md-6">
					<b>Description</b>
				</div>
				<hr>
			</div>
			
			<% @SuppressWarnings("unchecked")
			List<Tests> list = (List<Tests>) request.getAttribute("testList"); 
			for(Tests t : list){  %>
				<div class="row">
					<div class="col-md-2"><%= t.getName() %></div>
					<div class="col-md-6"><%= t.getDescription() %></div>
					<div class="col-md-1"><a href="takeQuiz?testID=<%= t.getId() %>">Take Quiz</a></div>
				</div>
			<% } %>
			
		</div>
		<% /*} catch (Exception e) { response.sendRedirect("login.jsp"); } */ %>
	</body>
</html>