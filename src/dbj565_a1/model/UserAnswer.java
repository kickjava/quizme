package dbj565_a1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
//import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**This class holds all the user entered/selected answers. It has an id column, QUESTION_ID to tell us which question the 
 * answer belongs to, USER_ID so we know who answered and the answer entered by the user.*/
@Entity
public class UserAnswer implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(nullable = true) //primary answer
	private String answer;
	
	@Column(nullable = true) //checkbox multiple answers
	private String answer2;
	
	@Column(nullable = true) //checkbox multiple answers
	private String answer3;
	
	@Column(nullable = true) //checkbox multiple answers
	private String answer4;

	public UserAnswer() {
		super();
	}
	
	public UserAnswer(String answer, String answer2, String answer3, String answer4) {
		super();
		this.answer = answer;
		this.answer2 = answer2;
		this.answer3 = answer3;
		this.answer4 = answer4;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAnswer2() {
		return answer2;
	}

	public void setAnswer2(String answer2) {
		this.answer2 = answer2;
	}

	public String getAnswer3() {
		return answer3;
	}

	public void setAnswer3(String answer3) {
		this.answer3 = answer3;
	}

	public String getAnswer4() {
		return answer4;
	}

	public void setAnswer4(String answer4) {
		this.answer4 = answer4;
	}
	
	
}
