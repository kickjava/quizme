package dbj565_a1.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**This class holds all the details regarding each result. It has an id variable, RESULT_ID, so it knows which result it 
 * belongs to, a questionID, which question is this detail of, and which answer the user selected, by USER_ANSWER_ID*/
@Entity
public class ResultDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	private Questions questionID;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private UserAnswer userAnswerID;
	
	public ResultDetails(){
		super();
	}
	public ResultDetails(Questions questionID, UserAnswer userAnswerID){
		super();
		this.questionID = questionID;
		this.userAnswerID = userAnswerID;
	}
	/**Setters and Getters*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Questions getQuestionID() {
		return questionID;
	}
	public void setQuestionID(Questions questionID) {
		this.questionID = questionID;
	}
	public UserAnswer getUserAnswerID() {
		return userAnswerID;
	}
	public void setUserAnswerID(UserAnswer userAnswerID) {
		this.userAnswerID = userAnswerID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((questionID == null) ? 0 : questionID.hashCode());
		result = prime * result + ((userAnswerID == null) ? 0 : userAnswerID.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultDetails other = (ResultDetails) obj;
		if (id != other.id)
			return false;
		if (questionID == null) {
			if (other.questionID != null)
				return false;
		} else if (!questionID.equals(other.questionID))
			return false;
		if (userAnswerID == null) {
			if (other.userAnswerID != null)
				return false;
		} else if (!userAnswerID.equals(other.userAnswerID))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ResultDetails [id=" + id + ", questionID=" + questionID + ", userAnswerID=" + userAnswerID + "]";
	}
	
	
}
