package dbj565_a1.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**This class holds all the questions for the online quiz. It has an id, a reference of TEST_ID, so it knows which question
 * belong to which test(s), type of question such as multiple choice, fill in the blanks, etc, question itself, hint, 
 * a reference of DIFFICULTY_ID, because each question has a difficulty level, and a reference of the answer class (ANSWER_ID).*/
@Entity
public class Questions implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@ManyToOne()
	private Tests testID;
	
	@Column(nullable = false)
	private String type;
	
	@Column(nullable = false)
	private String question;
	
	@Column(nullable = true)
	private String hint;
	
	@ManyToOne()
	private Difficulty difficultyID;
	
	
	@OneToMany(targetEntity=Options.class, cascade=CascadeType.ALL)
	private List<Options> options;
	
	@OneToMany(targetEntity=Answers.class, cascade=CascadeType.ALL)
	private List<Answers> answerID;
	
	public Questions(){
		super();
		options = new ArrayList<Options>();
		answerID = new ArrayList<Answers>();
	}
	public Questions(int id, Tests testID, String type, String question, String hint, Difficulty difficultyID, List<Answers> answerID){
		super();
		this.id = id;
		this.testID = testID;
		this.type = type;
		this.question = question;
		this.hint = hint;
		this.difficultyID = difficultyID;
		options = new ArrayList<Options>();
		answerID = new ArrayList<Answers>();
	}
	/**Setters and Getters*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Tests getTestID() {
		return testID;
	}
	public void setTestID(Tests testID) {
		this.testID = testID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getHint() {
		return hint;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	public Difficulty getDifficultyID() {
		return difficultyID;
	}
	public void setDifficultyID(Difficulty difficultyID) {
		this.difficultyID = difficultyID;
	}
	public List<Options> getOptions() {
		return options;
	}
	public void addOptions(Options options) {
		this.options.add(options);
	}
	public List<Answers> getAnswerID() {
		return answerID;
	}
	public void addAnswerID(Answers answerID) {
		this.answerID.add(answerID);
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answerID == null) ? 0 : answerID.hashCode());
		result = prime * result + ((difficultyID == null) ? 0 : difficultyID.hashCode());
		result = prime * result + ((hint == null) ? 0 : hint.hashCode());
		result = prime * result + id;
		result = prime * result + ((question == null) ? 0 : question.hashCode());
		result = prime * result + ((testID == null) ? 0 : testID.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Questions other = (Questions) obj;
		if (answerID == null) {
			if (other.answerID != null)
				return false;
		} else if (!answerID.equals(other.answerID))
			return false;
		if (difficultyID == null) {
			if (other.difficultyID != null)
				return false;
		} else if (!difficultyID.equals(other.difficultyID))
			return false;
		if (hint == null) {
			if (other.hint != null)
				return false;
		} else if (!hint.equals(other.hint))
			return false;
		if (id != other.id)
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (testID == null) {
			if (other.testID != null)
				return false;
		} else if (!testID.equals(other.testID))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Questions [id=" + id + ", testID=" + testID + ", type=" + type + ", question=" + question + ", hint="
				+ hint + ", difficultyID=" + difficultyID + ", answerID=" + answerID + "]";
	}
	
}
