package dbj565_a1.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**This class holds options to questions. For example: a multiple choice question will have options, which
 * will be stored here in the options variable. This class also has a reference/foreign key object of the 
 * Question (QUESTION_ID) class, this way we know which question the option belongs to and which Question 
 * this Test (TEST_ID) belongs to.*/

@Entity
public class Options implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="ID")
	private int id;
	
	
	@Column(nullable = false)
	private String options;
	
	public Options(){
		super();
	}
	public Options(String options) {
		super();
		this.options = options;
	}
	/**Setters and Getters*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOptions() {
		return options;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((options == null) ? 0 : options.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Options other = (Options) obj;
		if (id != other.id)
			return false;
		if (options == null) {
			if (other.options != null)
				return false;
		} else if (!options.equals(other.options))
			return false;
				return true;
	}
	@Override
	public String toString() {
		return "Options [id=" + id + ", options=" + options + "]";
	}
	
}
