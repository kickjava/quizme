package dbj565_a1.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**This class holds the results of the tests for each user. It has an id variable, a foreign key by USER_ID, which
 * will help it identify which user the result belongs to and a TEST_ID, which test does this result belong to.*/
@Entity
public class Results implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@ManyToOne
	private Tests testID;
	
	@ManyToOne
	private Users userID;
	
	@OneToMany(targetEntity=ResultDetails.class, cascade=CascadeType.ALL)
	private List<ResultDetails> resultDetails;
	
	@Column(nullable = false)
	private double score_result;
	
	public Results(){
		super();
	}
	public Results(Tests testID, Users userID, double score_result){
		super();
		this.testID = testID;
		this.userID = userID;
		this.score_result = score_result;
	}
	/**Setters and Getters*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Tests getTestID() {
		return testID;
	}
	public void setTestID(Tests testID) {
		this.testID = testID;
	}
	public Users getUserID() {
		return userID;
	}
	public void setUserID(Users userID) {
		this.userID = userID;
	}
	public double getScore_result() {
		return score_result;
	}
	public void setScore_result(double score_result) {
		this.score_result = score_result;
	}
	public List<ResultDetails> getResultDetails() {
		return resultDetails;
	}
	public void setResultDetails(List<ResultDetails> resultDetails) {
		this.resultDetails = resultDetails;
	}
	public void addResultDetails(ResultDetails resultDetails) {
		this.resultDetails.add(resultDetails);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		long temp;
		temp = Double.doubleToLongBits(score_result);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((testID == null) ? 0 : testID.hashCode());
		result = prime * result + ((userID == null) ? 0 : userID.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Results other = (Results) obj;
		if (id != other.id)
			return false;
		if (Double.doubleToLongBits(score_result) != Double.doubleToLongBits(other.score_result))
			return false;
		if (testID == null) {
			if (other.testID != null)
				return false;
		} else if (!testID.equals(other.testID))
			return false;
		if (userID == null) {
			if (other.userID != null)
				return false;
		} else if (!userID.equals(other.userID))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Results [id=" + id + ", testID=" + testID + ", userID=" + userID + ", score_result=" + score_result
				+ "]";
	}
	
}
