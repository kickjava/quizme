package dbj565_a1.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**This class holds the tests. It has an id, the name of the test is held in the name variable, and it has a description
 * which can contain things such as subject the test is based on.*/
@Entity
public class Tests implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(nullable = false)
	private String name;
	
	@Column(nullable = false)
	private String description;
	
	@ManyToOne
	@JoinColumn(name="USER_ID")
	private Users createdby;
	
	@Column(nullable = false)
	private boolean publish;
	
	public Tests(){
		super();
	}
	public Tests(String name, String description, Users user){
		super();
		this.name = name;
		this.description = description;
		this.createdby = user;
		publish = false;
	}
	/**Setters and Getters*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Users getCreatedby() {
		return createdby;
	}
	public void setCreatedby(Users createdby) {
		this.createdby = createdby;
	}
	public boolean isPublish() {
		return publish;
	}
	public void setPublish(boolean publish) {
		this.publish = publish;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((createdby == null) ? 0 : createdby.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (publish ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tests other = (Tests) obj;
		if (createdby == null) {
			if (other.createdby != null)
				return false;
		} else if (!createdby.equals(other.createdby))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (publish != other.publish)
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "Tests [id=" + id + ", name=" + name + ", description=" + description + ", createdby=" + createdby
				+ ", publish=" + publish + "]";
	}
	
}
