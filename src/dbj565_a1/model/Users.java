package dbj565_a1.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**This class holds the user's and their information, such as their full name, email and password.*/
@Entity
@Table(name="USERS", uniqueConstraints = {@UniqueConstraint(columnNames={"email"})})
public class Users implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static enum ROLES {ADMIN, PROFESSOR, STUDENT};

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(nullable = false)
	private String fullName;
	
	@Column(nullable = false)
	private String email;
	
	@Column(nullable = false)
	private String password;
	
	@Column(nullable = false)
	private String role;
	
	public Users(){
		super();
	}
	public Users(String fullName, String email, String password, ROLES role){
		super();
		this.fullName = fullName;
		this.email = email;
		this.password = password;
		if(role.equals(ROLES.ADMIN)) this.role = "ADMIN";
		if(role.equals(ROLES.PROFESSOR)) this.role = "PROFESSOR";
		if(role.equals(ROLES.STUDENT)) this.role = "STUDENT";
	}
	/**Setters and Getters*/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(ROLES role) {
		if(role.equals(ROLES.ADMIN)) this.role = "ADMIN";
		if(role.equals(ROLES.PROFESSOR)) this.role = "PROFESSOR";
		if(role.equals(ROLES.STUDENT)) this.role = "STUDENT";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((fullName == null) ? 0 : fullName.hashCode());
		result = prime * result + id;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (fullName == null) {
			if (other.fullName != null)
				return false;
		} else if (!fullName.equals(other.fullName))
			return false;
		if (id != other.id)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Users [id=" + id + ", fullName=" + fullName + ", email=" + email + ", password=" + password + "]";
	}
	
}
