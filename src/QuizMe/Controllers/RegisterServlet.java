package QuizMe.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.User;
import dbj565_a1.model.Users;
import dbj565_a1.model.Users.ROLES;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet(urlPatterns = {"/RegisterServlet", "/register", "/Register"})
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//redirecting to register page
		HttpSession session = request.getSession(false);
		try{ System.out.println("User name is: " + session.getAttribute("username"));
			if(session.getAttribute("username") != null){
				session.invalidate();
				response.sendRedirect("login.jsp");
			} else {
				getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
			}
		} catch(Exception e){
			System.out.println(e.getMessage());
			getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean check = true;
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String role = request.getParameter("type");
		
		String nameError = "";
		String emailError = "";
		String passwordError = "";
		
		if(name.trim().equals("")) {
			check = false;
			nameError = "Full Name cannot be empty.";
		} else if(!name.trim().matches("^[A-Za-z ,.'-]+$")) {
			check = false;
			nameError = "Full Name must not contain numbers and symbols.";
		}
		
		if(email.trim().equals("")) {
			check = false;
			emailError = "Email cannot be empty.";
		} else if(!email.trim().matches("^[a-zA-Z -_\\.]*(senecacollege.ca|myseneca.ca)$")) {
			check = false;
			emailError = "Invalid email: must use seneca email.";
		}
		
		if(password.trim().equals("")) {
			check = false;
			passwordError = "Passoword cannot be empty";
		} else if(password.trim().length() < 6) {
			check = false;
			passwordError = "password must be atleast 6 characters long.";
		}
		
		if(check){
			ROLES r = null;
			if(role.equalsIgnoreCase("Professor")) r = ROLES.PROFESSOR;
			if(role.equalsIgnoreCase("Student")) r = ROLES.STUDENT;
			

			
			Users user = new Users(name, email, password, r); //New user to register.
			
			User persistUser = new User();
			if(persistUser.addUser(user)){
				getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
			} else {
				request.setAttribute("persistError", "Failed to register new user.");
				getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
			}
		} else {
			request.setAttribute("nameError", nameError);
			request.setAttribute("emailError", emailError);
			request.setAttribute("passwordError", passwordError);
		    getServletContext().getRequestDispatcher("/register.jsp").forward(request, response);
		}
	}

}
