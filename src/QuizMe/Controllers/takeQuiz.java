package QuizMe.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.Answer;
import QuizMe.persistence.Option;
import QuizMe.persistence.Result;
import QuizMe.persistence.Test;
import QuizMe.persistence.User;
import QuizMe.persistence.UserAnswers;
import dbj565_a1.model.Answers;
import dbj565_a1.model.Options;
import dbj565_a1.model.Questions;
import dbj565_a1.model.ResultDetails;
import dbj565_a1.model.Results;
import dbj565_a1.model.UserAnswer;
import dbj565_a1.model.Users;

/**
 * Servlet implementation class takeQuiz
 */
@WebServlet({ "/takeQuiz", "/takequiz", "/TakeQuiz" })
public class takeQuiz extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	private Test test = new Test();

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) { 
			try{
				if(session.getAttribute("role").equals("STUDENT")){			
					if(test == null) {
						test = new Test();
					}
					int id = Integer.parseInt(request.getParameter("testID"));
					List<Questions> questions = test.makeTest(id);
					
					if(questions != null) {
						System.out.println("# of Questions: " + questions.size());
						request.setAttribute("listOfQuestions", questions);
						
					    session.setAttribute("qNumber", 0);
						getServletContext().getRequestDispatcher("/takeQuiz.jsp").forward(request, response);
					} else {
						response.sendRedirect(request.getContextPath() + "/TestServlet");
					}
				} else { 
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(NumberFormatException e) { 
				response.sendRedirect(request.getContextPath() + "/TestServlet"); 
			} catch (Exception e) { System.out.println("Exception is: " + e.getMessage());
				getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
			}
		} else {
			response.getWriter().append("Session timed out: please log in again.");
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		//List of questions
		@SuppressWarnings("unchecked")
		List<Questions> question = (List) request.getSession().getAttribute("questionList");
		
		//Question Number
		int qNum = (int) request.getSession().getAttribute("qNum");

		//Getting user answer
		String answer = request.getParameter("answer").toUpperCase();
		
		//TestID
		String testID = (String) request.getSession().getAttribute("test");
		
		/*System.out.println("Size: " + question.size());
		System.out.println("QNum: " + qNum);
		System.out.println("Answer: " + answer);
		System.out.println("TestID: " + testID);
		*/
		
		UserAnswer u = new UserAnswer();
		//Text/Number
		if(question.get(qNum).getType().equals("text") || question.get(qNum).getType().equals("number")){
			u.setAnswer(answer);
		}
		//Mcq
		else if(question.get(qNum).getType().equals("mcq")){
			List<Options> l = question.get(qNum).getOptions();
			if(answer.equalsIgnoreCase("OptionA")){
				u.setAnswer(l.get(0).getOptions().toUpperCase());
			}
			if(answer.equalsIgnoreCase("OptionB")){
				u.setAnswer2(l.get(1).getOptions().toUpperCase());
			}
			if(answer.equalsIgnoreCase("OptionC")){
				u.setAnswer3(l.get(2).getOptions().toUpperCase());
			}
			if(answer.equalsIgnoreCase("OptionD")){
				u.setAnswer4(l.get(3).getOptions().toUpperCase());
			}
		}
		//Checkbox
		else if(question.get(qNum).getType().equals("checkbox")){
			List<Options> l = question.get(qNum).getOptions();
			String[] checked = request.getParameterValues("answer");
			for(int i = 0; i < checked.length; i++){
				if(checked[i].equalsIgnoreCase("OptionA")){
					System.out.println("options: ");
					System.out.println(l.get(0).getId());
					System.out.println(l.get(0).getOptions());
					u.setAnswer(l.get(0).getOptions().toUpperCase());
				}
				if(checked[i].equalsIgnoreCase("OptionB")){
					u.setAnswer2(l.get(1).getOptions().toUpperCase());
				}
				if(checked[i].equalsIgnoreCase("OptionC")){
					u.setAnswer3(l.get(2).getOptions().toUpperCase());
				}
				if(checked[i].equalsIgnoreCase("OptionD")){
					u.setAnswer4(l.get(3).getOptions().toUpperCase());
				}
			}
		}
		//List/select
		else if(question.get(qNum).getType().equals("select")){
			String[] a = request.getParameterValues("answer"); //Multiple selection
System.out.println("User select answer is : " + a);			
			for(int i = 0; i < a.length; i++){
				u.setAnswer(a[i].toUpperCase());
			}
		}
		//Store it in DB
		HttpSession session = request.getSession(false);
		if(session.getAttribute("resultDetails") != null) {
			List<ResultDetails> rd = (List<ResultDetails>) session.getAttribute("resultDetails");
			rd.add(new ResultDetails(question.get(qNum), u));
			session.setAttribute("resultDetails", rd);
		} else {
			List<ResultDetails> rd = new ArrayList<ResultDetails>();
			rd.add(new ResultDetails(question.get(qNum), u));
			session.setAttribute("resultDetails", rd);
		}

		
/*		UserAnswers a = new UserAnswers();
		a.writeUserAnswer(u);
*/		
		qNum++;
		System.out.println("Size of questions: " + question.size());
		System.out.println("QNum: " + qNum);
		if(qNum < question.size()){
			//HttpSession session = request.getSession(false); //retrieving session
			if(session != null){
				session.setAttribute("qNum", qNum);
				//Use redirect instead of forward
				String url = request.getContextPath() + "/takeQuiz?testID="+testID;
				response.sendRedirect(url);
			}
		}
		else{
			//Redirect to main screen for user/results page?
			List<ResultDetails> rd = (List<ResultDetails>) session.getAttribute("resultDetails");
			Results result = new Results();
			
			//HttpSession session = request.getSession(false);
			result.setTestID(question.get(0).getTestID());
			int uid = (int) session.getAttribute("userID");
			Users user = new User().getUser(uid);
			result.setUserID(user);
			result.setResultDetails(rd);
			
			double quizresult = 0.0;
			
			for(int i=0; i < rd.size(); i++){
				ResultDetails r = rd.get(i);
				
				if(r.getQuestionID().getType().equals("text") || r.getQuestionID().getType().equals("number")){
					Answers correctanswer = r.getQuestionID().getAnswerID().get(0);
					String useranswer = r.getUserAnswerID().getAnswer();
					if(correctanswer.getAnswer().equals(useranswer)){
						quizresult += 1;
					}
				} else if(r.getQuestionID().getType().equals("mcq")){
					Answers correctanswer = r.getQuestionID().getAnswerID().get(0);
					String useranswer = r.getUserAnswerID().getAnswer();
					if(correctanswer.getAnswer().equals(useranswer)){
						quizresult += 1;
					}
				} else if(r.getQuestionID().getType().equals("checkbox")){
					List<Answers> correctanswer = r.getQuestionID().getAnswerID();
					String useranswer1 = r.getUserAnswerID().getAnswer();
					String useranswer2 = r.getUserAnswerID().getAnswer2();
					String useranswer3 = r.getUserAnswerID().getAnswer3();
					String useranswer4 = r.getUserAnswerID().getAnswer4();
					int checks = 0;
					if(!(useranswer1.isEmpty() && useranswer2.isEmpty() && useranswer3.isEmpty() && useranswer4.isEmpty())){
						if(correctanswer.size() == 4){ quizresult += 1; } //both selected all..
					}
					for(int t=0; t < correctanswer.size(); t++){
						if(correctanswer.get(t).getAnswer().equals(useranswer1)){ checks += 1;}
						if(correctanswer.get(t).getAnswer().equals(useranswer2)){ checks += 1;}
						if(correctanswer.get(t).getAnswer().equals(useranswer3)){ checks += 1;}
						if(correctanswer.get(t).getAnswer().equals(useranswer4)){ checks += 1;}
					}
					quizresult += checks/4;
				} else if(r.getQuestionID().getType().equals("select")){
					Answers correctanswer = r.getQuestionID().getAnswerID().get(0);
					String useranswer = r.getUserAnswerID().getAnswer();
					if(correctanswer.getAnswer().equals(useranswer)){
						quizresult += 1;
					}
				}
			}
			
			result.setScore_result(quizresult);
			Result persistResult = new Result();
			persistResult.writeResultToDB(result);

			response.sendRedirect("result?test=" + result.getTestID().getName() + "&result=" + result.getScore_result());
			//request.setAttribute("result", result);
			//getServletContext().getRequestDispatcher("/result").forward(request, response);
		}
	}
}
