package QuizMe.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.Test;
import QuizMe.persistence.User;
import dbj565_a1.model.Tests;
import dbj565_a1.model.Users;

/**
 * Servlet implementation class ModifyTest
 */
@WebServlet(urlPatterns = {"/modifyTest", "/ModifyTest", "/modifytest"})
public class ModifyTest extends HttpServlet {
	private static final long serialVersionUID = 1L;
    //Persistance class.
	private Test tests = new Test();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) {
			try{ 
				if(session.getAttribute("role").equals("PROFESSOR")){
					String action = request.getParameter("action");
					String testid = request.getParameter("testID");
					String message = " ";
					
					int id = Integer.parseInt(testid); 
						
					if(tests == null) { tests = new Test(); }
						
					switch(action.toUpperCase()){
						case "PUBLISH": message = tests.publishTest(id);
							break;
						case "DELETE": message = tests.deleteTest(id);
							break;
					}
					
					request.setAttribute("message", message);
					getServletContext().getRequestDispatcher("/test").forward(request, response);
					
				}  else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(NumberFormatException e) { 
				String message = "test not recognized!";
				request.setAttribute("message", message);
				getServletContext().getRequestDispatcher("/test.jsp").forward(request, response);
			} catch(Exception e){ 
				getServletContext().getRequestDispatcher("/403.html").forward(request, response);
			}
		} else {
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) { 
			try{
				if(session.getAttribute("role").equals("PROFESSOR")){
					String testName = request.getParameter("testName");
					String testDescription = request.getParameter("testDescription");
					boolean check = true;
					String testNameError = " ";
					String descriptionError = " ";
					
					//Testname
					if(testName.isEmpty()){
						testNameError = "Test name cannot be empty.";
						check = false;
					}
					else{
						testNameError = "";
					}
					
					//Description
					if(testDescription.isEmpty()){
						descriptionError = "Description cannot be empty.";
						check = false;
					}
					else{
						descriptionError = "";
					}
					
					if(!check){
						request.setAttribute("testNameError", testNameError);
						request.setAttribute("descriptionError", descriptionError);
					    getServletContext().getRequestDispatcher("/editTest.jsp?testID=" + request.getParameter("testID") + "&Name=" + testName + "&Desc=" + testDescription)
					    .forward(request, response);
					}
					else{
						//Writing test
						if(tests == null) {
							tests = new Test();
						}
						Tests test2 = new Tests();
						test2.setName(request.getParameter("testName"));
						test2.setDescription(request.getParameter("testDescription"));
						test2.setId(Integer.parseInt(request.getParameter("testID")));

						tests.updateTest(test2);
						request.setAttribute("testID", String.valueOf(test2.getId()));

						//getServletContext().getRequestDispatcher("/test").forward(request, response);
						response.sendRedirect("test");
					}
				} else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(Exception e) {
				getServletContext().getRequestDispatcher("/403.html").forward(request, response);
			}
		} else {
			response.getWriter().append("Session timed out: please log in again.");
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

}
