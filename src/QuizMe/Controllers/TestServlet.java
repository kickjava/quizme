package QuizMe.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.Difficult;
import QuizMe.persistence.Test;
import QuizMe.persistence.User;
import dbj565_a1.model.Difficulty;
import dbj565_a1.model.Tests;
import dbj565_a1.model.Users;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet(urlPatterns = {"/TestServlet", "/test", "/Test"})
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Test tests = new Test();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) { 
			try{ 
				if(session.getAttribute("role").equals("PROFESSOR")){
					if(tests == null) {
						tests = new Test();
					}
					
					List<Tests> alltest = tests.getAllTests((int)session.getAttribute("userID"));
					request.setAttribute("testList", alltest);
					
					getServletContext().getRequestDispatcher("/test.jsp").forward(request, response);
				} else if(session.getAttribute("role").equals("STUDENT")){			
					List<Tests> alltest = tests.getAllTests();
					request.setAttribute("testList", alltest);
					
					getServletContext().getRequestDispatcher("/listtest.jsp").forward(request, response);
				} else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(Exception e) {
				getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
			}
		} else {
			response.getWriter().append("Session timed out: please log in again.");
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) { 
			try{
				if(session.getAttribute("role").equals("PROFESSOR")){
					response.setContentType("text/html");
					PrintWriter out = response.getWriter();
					
					String testName = request.getParameter("testName");
					String testDescription = request.getParameter("testDescription");
					boolean check = true;
					String testNameError = " ";
					String descriptionError = " ";
					
					//Testname
					if(testName.trim().equals("")){
						testNameError = "Test name cannot be empty.";
						check = false;
					}
					else{
						testNameError = "";
					}
					
					//Description
					if(testDescription.trim().equals("")){
						descriptionError = "Description cannot be empty.";
						check = false;
					}
					else{
						descriptionError = "";
					}
					
					if(!check){
						request.setAttribute("testNameError", testNameError);
						request.setAttribute("descriptionError", descriptionError);
					    getServletContext().getRequestDispatcher("/test.jsp").forward(request, response);
					}
					else{
						//Writing test
						if(tests == null) {
							tests = new Test();
						}
						Tests test2 = new Tests();
						test2.setName(request.getParameter("testName"));
						test2.setDescription(request.getParameter("testDescription"));
						//adding user to test for permission purpose.
						User userpersist = new User(); 
						Users u = userpersist.getUser((int)session.getAttribute("userID"));
						test2.setCreatedby(u);
						userpersist.close();
						
						test2.setId(tests.createTest(test2));
						request.setAttribute("testID", String.valueOf(test2.getId()));
						request.setAttribute("testName", testName);
						request.setAttribute("testDescription", testDescription);
						
						//getServletContext().getRequestDispatcher("/test").forward(request, response);
						response.sendRedirect("createQuestion.jsp?testID=" + test2.getId());
					}
				} else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(Exception e) {
				getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
			}
		} else {
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

}
