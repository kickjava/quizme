package QuizMe.Controllers;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.Question;
import QuizMe.persistence.Test;
import dbj565_a1.model.Questions;

/**
 * Servlet implementation class proffResult
 */
@WebServlet("/proffResult")
public class proffResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public proffResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if(session != null) { 
			try{
				if(session.getAttribute("role").equals("PROFESSOR")){ 
					Test t = new Test();
					int tid = Integer.parseInt((String) request.getParameter("testID"));			
					List<Double> allresults = t.getAllTestScore(tid);
					
					if(allresults == null) {
						String message = "No test result is found.";
						request.setAttribute("message", message);
						getServletContext().getRequestDispatcher("/test").forward(request, response);
					}
					
					Collections.sort(allresults);
					request.setAttribute("results", allresults);
					
					getServletContext().getRequestDispatcher("/proffresult.jsp").forward(request, response);
				} else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(Exception e) { System.out.println("Exception from proffResult is... " + e.getMessage());
				getServletContext().getRequestDispatcher("/test").forward(request, response);
				//response.sendRedirect("test");
			}
	} else {
		getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
