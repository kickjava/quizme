package QuizMe.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.User;
import dbj565_a1.model.Users;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(urlPatterns = {"/LoginServlet", "/login", "/Login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		boolean check = true;
		String usernameError = " ";
		String passwordError = " ";
		if(username.isEmpty()){
			usernameError = "Username cannot be empty.";
			check = false;
		}
		else if(!username.trim().matches("^[a-zA-Z -_\\.]*(senecacollege.ca|myseneca.ca)$")){
			usernameError = "Invalid email: must use seneca email.";
			check = false;
		}
		else{
			usernameError = "";
		}
		if(password.isEmpty()){
			passwordError = "Password cannot be empty.";
			check = false;
		}
		else{
			passwordError = "";
		}
		if(check == false){
			request.setAttribute("usernameError", usernameError);
			request.setAttribute("passwordError", passwordError);
		    getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	    if(check){
	    	User user = new User(); //persistance class
	    	Users u = user.checkUser(username,password); //model class
	    	
	    	if(u != null){
	    		request.setAttribute("username", u.getFullName());
	    		
	    		HttpSession session = request.getSession(); //for security purpose.
	    		session.setAttribute("role", u.getRole());
	    		session.setAttribute("username", u.getFullName());
	    		session.setAttribute("userID", u.getId());
				
	    		response.sendRedirect(request.getContextPath() + "/TestServlet");
				//getServletContext().getRequestDispatcher("/test.jsp").forward(request, response);
	    	}
	    	else{
	    		usernameError = "Invalid username or password.";
	    		passwordError = "Invalid username or password.";
	    		
	    		request.setAttribute("usernameError", usernameError);
				request.setAttribute("passwordError", passwordError);
			    getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
	    	}
	    }
	}
}
