package QuizMe.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.Question;
import QuizMe.persistence.Test;

/**
 * Servlet implementation class deleteQuestion
 */
@WebServlet("/deleteQuestion")
public class deleteQuestion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public deleteQuestion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) {
			try{ 
				if(session.getAttribute("role").equals("PROFESSOR")){
					String question = request.getParameter("id");
					String message = " ";
					
					int id = Integer.parseInt(question); 
					Question persistQ = new Question();	
					persistQ.deleteQuestion(id);
					
					String testid = (String) request.getParameter("testID");
					response.sendRedirect("createQuestion.jsp?testID=" + testid);
					//getServletContext().getRequestDispatcher("/createQuestion.jsp?testID=" + testid).forward(request, response);
					
				} else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(NumberFormatException e) { 
				String testid = (String) request.getParameter("testID");
				
				getServletContext().getRequestDispatcher("/createQuestion.jsp?testID=" + testid).forward(request, response);
			} catch(Exception e){ 
				getServletContext().getRequestDispatcher("/403.html").forward(request, response);
			}
		} else {
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
