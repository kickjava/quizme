package QuizMe.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import QuizMe.persistence.Answer;
import QuizMe.persistence.Difficult;
import QuizMe.persistence.Option;
import QuizMe.persistence.Question;
import QuizMe.persistence.Test;
import dbj565_a1.model.Answers;
import dbj565_a1.model.Difficulty;
import dbj565_a1.model.Options;
import dbj565_a1.model.Questions;
import dbj565_a1.model.Tests;
import dbj565_a1.model.UserAnswer;

/**
 * Servlet implementation class createQuestion
 */
@WebServlet("/createQuestion")
public class createQuestion extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private Question q = new Question(); 
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public createQuestion() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) { 
			try{
				if(session.getAttribute("role").equals("PROFESSOR")){ 
					if(q == null) {
						q = new Question();
					}

					String testID = (String)request.getParameter("testID");
					List<Questions> questions = q.getAllQuestions(testID);
System.out.println("Total questions are: " + questions.size());
					request.setAttribute("questionPool", questions);

					getServletContext().getRequestDispatcher("/createQuestion.jsp").forward(request, response);
				} else {
					getServletContext().getRequestDispatcher("/403.html").forward(request, response);
				}
			} catch(Exception e) {
				getServletContext().getRequestDispatcher("/test").forward(request, response);
				//response.sendRedirect("test");
			}
		} else {
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		HttpSession session = request.getSession(false); //retrieving session
		if(session != null) { 
			try{
				if(session.getAttribute("role").equals("STUDENT")){
					response.sendRedirect("test.jsp");
				}
				
				if(session.getAttribute("role").equals("PROFESSOR")){
					response.setContentType("text/html");
					
					//Error checking..
					boolean check = true;
					String answerError = "";
					String questionError = "";
					String optionError = "";
					Difficulty easy = new Difficulty(1, "easy");
					Difficulty med = new Difficulty(2, "medim");
					Difficulty hard = new Difficulty(3, "hard");
					Tests test = new Tests();
					
					//Set QuestionID First
					Questions question = new Questions();
					String q = request.getParameter("question"); //question
					String h = request.getParameter("hint");
					
					//Set testID for question
					if(request.getParameter("testID") != null){
						Test t = new Test();
						test = t.getTest(Integer.parseInt(request.getParameter("testID")));
						question.setTestID(test);
					}
					else{
						check = false;
					}
					System.out.println("Question Testid: " + question.getTestID());
					
					
					String questionType = request.getParameter("questionChoose");
					
					switch(questionType){
						case "text":
							question.setType("text");
							
							if(q.trim().equals("")) {
								questionError = "question cannot be empty!";
								check = false;
							} else {
								questionError = "";
								question.setQuestion(q);
							}
							
							question.setHint(h); //adding hint to question
							
							String ans = request.getParameter("answer");
							if(ans.trim().equals("")){
								answerError = "Answer cannot be empty!";
								check = false;
							}
							
							if(request.getParameter("difficult").equals("easy")){
								question.setDifficultyID(easy);
							} else if(request.getParameter("difficult").equals("medium")){
								question.setDifficultyID(med);
							} else {
								question.setDifficultyID(hard);
							}
							
							
							if(check){
								//Persist answer
								Answers answer = new Answers(ans.toUpperCase());
								//Answer persistAns = new Answer();
								//persistAns.writeAnswerToDB(answer);
								question.addAnswerID(answer);
								
								//Persist question
								Question persistQ = new Question();
								persistQ.writeQuestionToDB(question);
								
							} else {
								request.setAttribute("questionError", questionError);
								request.setAttribute("answerError", answerError);
							}
							
							doGet(request, response);
							break;
							
						case "number": 
							question.setType("number");
							
							if(q.trim().equals("")) {
								questionError = "question cannot be empty!";
								check = false;
							} else {
								questionError = "";
								question.setQuestion(q);
							}
							question.setHint(h);
							ans = request.getParameter("answer");
							if(ans.trim().equals("")){
								answerError = "Answer cannot be empty!";
								check = false;
							}
							
							if(request.getParameter("difficult").equals("easy")){
								question.setDifficultyID(easy);
							} else if(request.getParameter("difficult").equals("medium")){
								question.setDifficultyID(med);
							} else {
								question.setDifficultyID(hard);
							}
							
							if(check){
								//Persist answer
								Answers answer = new Answers(ans);
								//Answer persistAns = new Answer();
								//persistAns.writeAnswerToDB(answer);
								question.addAnswerID(answer);
								
								//Persist question
								Question persistQ = new Question();
								persistQ.writeQuestionToDB(question);
								
							} else {
								request.setAttribute("questionError", questionError);
								request.setAttribute("answerError", answerError);
							}
							
							doGet(request, response);
							break;
							
						case "mcq": 
							question.setType("mcq");
							String option1 = request.getParameter("optionA");
							String option2 = request.getParameter("optionB");
							String option3 = request.getParameter("optionC");
							String option4 = request.getParameter("optionD");
							
							if(q.trim().equals("")) {
								questionError = "question cannot be empty!";
								check = false;
							} else {
								questionError = "";
								question.setQuestion(q);
							}
							question.setHint(h);
							
							if(option1.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option2.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option3.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option4.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							}
							
							ans = request.getParameter("answer");
			
							if(ans == null){
								answerError = "Must select one of the answer.";
								check = false;
							} else {
								if(ans.equalsIgnoreCase("optionA")){ ans = option1; }
								else if(ans.equalsIgnoreCase("optionB")){ ans = option2; }
								else if(ans.equalsIgnoreCase("optionC")){ ans = option3; }
								else if(ans.equalsIgnoreCase("option4")){ ans = option4; }
							}
							
							if(request.getParameter("difficult").equals("easy")){
								question.setDifficultyID(easy);
							} else if(request.getParameter("difficult").equals("medium")){
								question.setDifficultyID(med);
							} else {
								question.setDifficultyID(hard);
							}
							
							if(check){
								//Persist answer
								Answers answer = new Answers(ans.toUpperCase());
								//Answer persistAns = new Answer();
								//persistAns.writeAnswerToDB(answer);
								question.addAnswerID(answer);
								
								//Persist options
								Options opt1 = new Options(option1.trim());
								Options opt2 = new Options(option2.trim());
								Options opt3 = new Options(option3.trim());
								Options opt4 = new Options(option4.trim());
							//Option persistopt = new Option();

								//adding options to questions
								question.addOptions(opt1);
								question.addOptions(opt2);
								question.addOptions(opt3);
								question.addOptions(opt4);
								
								//Persist Question
								Question persistQ = new Question();
								persistQ.writeQuestionToDB(question);
								
							} else {
								request.setAttribute("questionError", questionError);
								request.setAttribute("optionError", optionError);
								request.setAttribute("answerError", answerError);
							}
							
							doGet(request, response);
							break;
							
						case "checkbox": 
							question.setType("checkbox");
							
							option1 = request.getParameter("optionA");
							option2 = request.getParameter("optionB");
							option3 = request.getParameter("optionC");
							option4 = request.getParameter("optionD");
							
							if(q.trim().equals("")) {
								questionError = "question cannot be empty!";
								check = false;
							} else {
								questionError = "";
								question.setQuestion(q);
							}
							question.setHint(h);
							
							if(option1.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option2.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option3.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option4.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							}
							
							String[] answers = request.getParameterValues("answer");

							if(answers == null){
								answerError = "Must select one of the answer.";
								check = false;
							} else {
								//adding all selected values to question answer list
								for(String s : answers){
									if(s.equalsIgnoreCase("optionA")){
										question.addAnswerID(new Answers(option1.trim().toUpperCase()));
									} else if(s.equalsIgnoreCase("optionB")){
										question.addAnswerID(new Answers(option2.trim().toUpperCase()));
									} else if(s.equalsIgnoreCase("optionC")){
										question.addAnswerID(new Answers(option3.trim().toUpperCase()));
									} else if(s.equalsIgnoreCase("optionD")){
										question.addAnswerID(new Answers(option4.trim().toUpperCase()));									
									}
								}
							}
							
							if(request.getParameter("difficult").equals("easy")){
								question.setDifficultyID(easy);
							} else if(request.getParameter("difficult").equals("medium")){
								question.setDifficultyID(med);
							} else {
								question.setDifficultyID(hard);
							}
							
							if(check){
								//Persist options
								Options opt1 = new Options(option1.trim());
								Options opt2 = new Options(option2.trim());
								Options opt3 = new Options(option3.trim());
								Options opt4 = new Options(option4.trim());
							//Option persistopt = new Option();

								//adding options to questions
								question.addOptions(opt1);
								question.addOptions(opt2);
								question.addOptions(opt3);
								question.addOptions(opt4);
								
								//Persist Question
								Question persistQ = new Question();
								persistQ.writeQuestionToDB(question);
								
							} else {
								request.setAttribute("questionError", questionError);
								request.setAttribute("optionError", optionError);
								request.setAttribute("answerError", answerError);
							}

							doGet(request, response);
							break;
						case "select": System.out.println("User choosed Select");
							question.setType("select");
							
							option1 = request.getParameter("optionA");
							option2 = request.getParameter("optionB");
							option3 = request.getParameter("optionC");
							option4 = request.getParameter("optionD");
							
							if(q.trim().equals("")) {
								questionError = "question cannot be empty!";
								check = false;
							} else {
								questionError = "";
								question.setQuestion(q);
							}
							question.setHint(h);
							
							if(option1.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option2.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option3.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							} else if(option4.trim().equals("")){
								optionError = "Option cannot be empty.";
								check = false;
							}
							
							ans = request.getParameter("answer");
			
							if(ans == null){
								answerError = "Must select one of the answer.";
								check = false;
							} else {
								if(ans.equalsIgnoreCase("optionA")){ ans = option1; }
								else if(ans.equalsIgnoreCase("optionB")){ ans = option2; }
								else if(ans.equalsIgnoreCase("optionC")){ ans = option3; }
								else if(ans.equalsIgnoreCase("option4")){ ans = option4; }
							}
							
							if(request.getParameter("difficult").equals("easy")){
								question.setDifficultyID(easy);
							} else if(request.getParameter("difficult").equals("medium")){
								question.setDifficultyID(med);
							} else {
								question.setDifficultyID(hard);
							}
							
							if(check){
								//Persist answer
								Answers answer = new Answers(ans.toUpperCase());
								//Answer persistAns = new Answer();
								//persistAns.writeAnswerToDB(answer);
								question.addAnswerID(answer);
								
								//Persist options
								Options opt1 = new Options(option1.trim());
								Options opt2 = new Options(option2.trim());
								Options opt3 = new Options(option3.trim());
								Options opt4 = new Options(option4.trim());
							//Option persistopt = new Option();

								//adding options to questions
								question.addOptions(opt1);
								question.addOptions(opt2);
								question.addOptions(opt3);
								question.addOptions(opt4);
								
								//Persist Question
								Question persistQ = new Question();
								persistQ.writeQuestionToDB(question);
								
							} else {
								request.setAttribute("questionError", questionError);
								request.setAttribute("optionError", optionError);
								request.setAttribute("answerError", answerError);
							}
							
							doGet(request, response);
							break;
					}

//doGet(request, response);
				}
			} catch(Exception e) {
				request.setAttribute("message", "Something went wrong, try again!");
				getServletContext().getRequestDispatcher("/test").forward(request, response);
			}
		} else {
			getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}
}
