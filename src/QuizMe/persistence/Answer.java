package QuizMe.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dbj565_a1.model.Answers;
import dbj565_a1.model.Questions;

public class Answer implements AutoCloseable{

	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public Answer(){
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public void writeAnswerToDB(Answers a){
		em.getTransaction().begin();
		em.persist(a);
		em.getTransaction().commit();
	}
	
	@Override
	public void close() throws Exception {		
		em.close();
	}
}
