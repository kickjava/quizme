package QuizMe.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dbj565_a1.model.Questions;
import dbj565_a1.model.Tests;

public class Question implements AutoCloseable{

	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public Question() {
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public void writeQuestionToDB(Questions q){
		em.getTransaction().begin();
		em.persist(q);
		em.getTransaction().commit();		
	}
	
	public int getQuestionID(){
		em.getTransaction().begin();
		Query q = em.createQuery("select t from Questions t");
		
		@SuppressWarnings("unchecked")
		List<Tests> ret = q.getResultList();
		
		em.getTransaction().commit();
		
		int qID = 0;
		List<Tests> lastQuestion = new ArrayList<Tests>();
		if(ret.size() > 0){
			qID = ret.size();
		}
		else if(ret.size() == 0){
			qID = 0;
		}
		
		return qID;
	}
	
	public List<Questions> getAllQuestions(String testID){
		em.getTransaction().begin();
		
		Tests t = em.find(Tests.class, Integer.parseInt(testID));
		Query q = em.createQuery("select q from Questions q "
				+ " where q.testID = :testID ORDER BY q.difficultyID ");
		q.setParameter("testID", t);

		@SuppressWarnings("unchecked")
		List<Questions> questions = q.getResultList();
		em.getTransaction().commit();
		return questions;
	}
	
	public void deleteQuestion(int id){
		Questions q = em.find(Questions.class, id);
		
		if(q != null) {
			em.getTransaction().begin();
			em.remove(q);
			em.getTransaction().commit();
		}
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		em.close();
	}
}
