package QuizMe.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dbj565_a1.model.Difficulty;
import dbj565_a1.model.Questions;
import dbj565_a1.model.Tests;
import dbj565_a1.model.Users;

public class Difficult implements AutoCloseable{

	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public Difficult() {
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public void writeDifficulty(Difficulty d){
		em.getTransaction().begin();
		
		Query q = em.createQuery("select t from Difficulty t where t.id = '" + d.getId() + "' and t.name = '" + d.getName() + "'");
		
		@SuppressWarnings("unchecked")
		List<Difficulty> ret = q.getResultList();
		
		if(ret.size() == 1){
			
		}
		else{
			em.persist(d);
		}
		em.getTransaction().commit();
	}
	
	public Difficulty getDifficulty(String name){
		Difficulty d = new Difficulty();
		em.getTransaction().begin();
		
		Query q = em.createQuery("select t from Difficulty t where t.name = '" + name + "'");
		
		@SuppressWarnings("unchecked")
		List<Difficulty> ret = q.getResultList();
		
		em.getTransaction().commit();
		if(ret.size() == 1){
			d.setId(ret.get(0).getId());
			d.setName(ret.get(0).getName());
		}
		return d;
	}
	
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub		
	}
}
