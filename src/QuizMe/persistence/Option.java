package QuizMe.persistence;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dbj565_a1.model.Options;
import dbj565_a1.model.Questions;
import dbj565_a1.model.Tests;

public class Option implements AutoCloseable {
	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public Option(){
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public void writeOptionToDB(Options opt){
		em.getTransaction().begin();
		em.persist(opt);
		em.getTransaction().commit();
	}
	
	public Options getDefault(){
		return em.find(Options.class, 1);
	}
	
	@Override
	public void close() throws Exception {		
		em.close();
	}
}
