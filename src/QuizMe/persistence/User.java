package QuizMe.persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import dbj565_a1.model.Users;

public class User implements AutoCloseable{

	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public User() {
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public Users checkUser(String username, String password){
		em.getTransaction().begin();
		
		Query q = em.createQuery("select t from Users t where t.email = '" + username + "' and t.password = '" + password + "'");
		
		@SuppressWarnings("unchecked")
		List<Users> ret = q.getResultList();
		
		em.getTransaction().commit();
		
		if(ret.size() == 1)
			return ret.get(0);
		else 
			return null;
	}
	
	public boolean addUser(Users user) {
		boolean check = false;
		em.getTransaction().begin();
		
		em.persist(user);
		if (em.contains(user)) { //checking if it persisted user
			  check = true;
		}
		
		em.getTransaction().commit();
		
		return check;
	}
	
	public Users getUser(int id) {
		em.getTransaction().begin();
		Users u = em.find(Users.class, id);
		
		em.getTransaction().commit();
		
		return u;
	}
	
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		em.close();
	}

}
