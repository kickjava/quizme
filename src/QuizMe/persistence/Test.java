package QuizMe.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import dbj565_a1.model.Questions;
import dbj565_a1.model.Difficulty;
import dbj565_a1.model.Tests;
import dbj565_a1.model.Users;

public class Test implements AutoCloseable{
	
	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public Test() {
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public List<Tests> getLastTestID(){
		em.getTransaction().begin();
		Query q = em.createQuery("select t from Tests t");
		
		@SuppressWarnings("unchecked")
		List<Tests> ret = q.getResultList();
		em.getTransaction().commit();
		
		List<Tests> lastTest = new ArrayList<Tests>();
		lastTest.add(ret.get(ret.size()-1));
		
		return lastTest;
	}
	
	public List<Tests> getAllTests(int userid){
		em.getTransaction().begin();
		Users u = em.find(Users.class, userid);
		Query q = em.createQuery("select t from Tests t where t.createdby = :user");
		q.setParameter("user", u);
		
		@SuppressWarnings("unchecked")
		List<Tests> ret = q.getResultList();
		
		em.getTransaction().commit();
		return ret;
	}
	
	public List<Tests> getAllTests(){
		em.getTransaction().begin();
		Query q = em.createQuery("select t from Tests t where t.publish = " + true);
		
		@SuppressWarnings("unchecked")
		List<Tests> ret = q.getResultList();
		
		em.getTransaction().commit();
		return ret;
	}
	
	public int createTest(Tests test){
		int ret = 0;
		em.getTransaction().begin();
		em.persist(test);
		ret = test.getId();
		em.getTransaction().commit();
		return ret;
	}
	
	public Tests getTest(int id) {
		return em.find(Tests.class, id);
	}
	
	public String publishTest(int id){
		String message = "successfylly updated!";
		em.getTransaction().begin();
		Tests t = em.find(Tests.class, id);
		if(t != null){
			if(t.isPublish()){
				t.setPublish(false);
			} else {
				Difficulty e = new Difficulty(1, "easy");
				Difficulty m = new Difficulty(2, "medium");
				Difficulty h = new Difficulty(3, "hard");
				
				Query q = em.createQuery("select count(q) from Questions q where q.testID = :test and q.difficultyID = :diff");
				q.setParameter("test", t);
				
				q.setParameter("diff", e);
				long easyquestions =  (long) q.getSingleResult();
				
				q.setParameter("diff", m);
				long medquestions =  (long) q.getSingleResult();
				q.setParameter("diff", h);
				long hardquestions =  (long) q.getSingleResult();
				if(easyquestions >= 5 && medquestions >= 3 && hardquestions >= 2){
					t.setPublish(true); //transparent update will update table as well
				} else { 
					message = "Not enough questions to publish, need atleast 5 easy, 3 medium, and 2 hard questions";
				}
			}
		} else {
			message = "Test not found!";
		}
	
		em.getTransaction().commit();
		
		return message;
	}
	
	//removing options, questions and test.
	public String deleteTest(int id){
		String message = "successfully removed test.";
		em.getTransaction().begin();
		Tests t = em.find(Tests.class, id);
		if(t != null){
			Query q = em.createQuery("DELETE FROM Questions where testID = :test");
			q.setParameter("test", t);
			q.executeUpdate();
			
			em.remove(t);
		} else {
			message = "Test not found!";
		}
		em.getTransaction().commit();
		
		return message;
	}
	
	public void updateTest(Tests test){
		Tests t = em.find(Tests.class, test.getId());
		em.getTransaction().begin();
		if(t != null) {
			t.setName(test.getName());
			t.setDescription(test.getDescription());
		}
		em.getTransaction().commit();
	}
	
	public List<Questions> makeTest(int testID){
		Tests t = em.find(Tests.class, testID);

		if(t == null) { return null; } 
		
		Difficulty e = new Difficulty(1, "easy");
		Difficulty m = new Difficulty(2, "medium");
		Difficulty h = new Difficulty(3, "hard");
		
		Query q = em.createQuery("select q from Questions q where q.testID = :test "
				+ " and q.difficultyID = :diff");
		q.setParameter("test", t);
		q.setParameter("diff", e);

		List<Questions> easyquestions = q.getResultList();

		q.setParameter("diff", m);
		List<Questions> medquestions= q.getResultList();
		
		q.setParameter("diff", h);
		List<Questions> hardquestions = q.getResultList();
		
		List<Questions> questions = new ArrayList<>();
		
		//adding random easy questions.
		java.util.Collections .shuffle(easyquestions);
		questions.add(easyquestions.get(0));
		questions.add(easyquestions.get(1));
		questions.add(easyquestions.get(2));
		
		//adding random medium questions.
		java.util.Collections .shuffle(medquestions);
		questions.add(medquestions.get(0));
		questions.add(medquestions.get(1));
		
		//adding random hard questions.
		java.util.Collections .shuffle(hardquestions);
		questions.add(hardquestions.get(0));
		
		//shuffling return list to make the list random.
		java.util.Collections .shuffle(questions);
		
		return questions;
	}
	
	public List<Double> getAllTestScore(int testid){
		Tests t = em.find(Tests.class, testid);
		if(t != null){

			Query q = em.createQuery("select r.score_result from Results r where r.testID = :test");
			q.setParameter("test", t);
			
			List<Double> ret = q.getResultList();

			return ret;
		} else {
			return null;
		}
	}
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		em.close();
	}
}
