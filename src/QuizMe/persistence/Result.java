package QuizMe.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dbj565_a1.model.Results;

public class Result implements AutoCloseable{
	
	private EntityManagerFactory factory; 
	private EntityManager em;

	public Result(){
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public void writeResultToDB(Results result){
		em.getTransaction().begin();
		em.persist(result);
		em.getTransaction().commit();
	}
	
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		em.close();
	}
}
