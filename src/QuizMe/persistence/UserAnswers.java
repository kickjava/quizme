package QuizMe.persistence;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dbj565_a1.model.UserAnswer;

public class UserAnswers implements AutoCloseable{

	private EntityManagerFactory factory; 
	private EntityManager em;
	
	public UserAnswers(){
		factory = Persistence.createEntityManagerFactory("Quiz-Me");
		em = factory.createEntityManager();
	}
	
	public void writeUserAnswer(UserAnswer u){
		em.getTransaction().begin();
		em.persist(u);
		em.getTransaction().commit();
	}
	
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
